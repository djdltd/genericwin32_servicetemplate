#include <windows.h>
#include <stdio.h>
#include "MemoryBuffer.h"
#include "EventDescriptions.h"

#define ID_TIMER 1000
#define SIZE_STRING	1024
#define WARNING		100
#define INFO				101
#define ERR				102

static char g_szFriendlyName[] = "CedeDrive Manager Service";
static char g_szServiceName[] = "CedeDriveManagerService";
static char g_szServiceID[] = "CedeDriveService";
static char g_szServiceDesc [] = "Manages the mounting and unmounting of Virtual Encrypted drives used by CedeDrive.";
static char g_szEventsource[] = "CedeDrive";

/*
REMEMBER! EventDescriptions.mc contains all the descriptions of the possible events this service can generate for the
windows event viewer. After you have added or modified events, you must recompile this file using the Message Compiler MC.
e.g. mc EventDescriptions.mc    - You must use the Visual Studio Command Line to do this. This is so that event viewer knows where
to look for the decriptions of your events, because every time you log and event, you log it using it's ID which is stored in the event descriptions
file. I don't see why Microsoft couldn't have just provided a simple ReportEvent function taking in a string, but no they want us to create a whole
separate event file, add a source to the registry and only then will event viewer see the descriptions.

This is now a pre build event in the visual studio project, so manual compiling should not be necessary
ALWAYS edit EventDescriptions.mc - do NOT edit EventDescriptions.h or EventDescriptions.rc. These are modified by the message compiler

*/

SERVICE_STATUS MyServiceStatus; 
SERVICE_STATUS_HANDLE MyServiceStatusHandle; 
HINSTANCE g_hInstance;

// Prototypes
void  WINAPI MyServiceStart (DWORD argc, LPTSTR *argv); 
void  WINAPI MyServiceCtrlHandler (DWORD opcode);
DWORD MyServiceInitialization (DWORD argc, LPTSTR *argv, DWORD *specificError); 
VOID SvcDebugOut(LPSTR String, DWORD Status);
int ServiceMain ();
VOID InstallService (LPCTSTR lpszBinaryPathName);
VOID UninstallService ();
void ReportEventLog (DWORD dwEventID, unsigned int iType);
BOOL AddEventSource(LPTSTR pszLogName, LPTSTR pszSrcName, DWORD dwNum);

// Implementation
VOID WINAPI MyServiceCtrlHandler (DWORD Opcode)  { 
	DWORD status; 
 
	switch(Opcode) 
	{ 
		case SERVICE_CONTROL_PAUSE: 
		// Do whatever it takes to pause here. 
		MyServiceStatus.dwCurrentState = SERVICE_PAUSED; 
		break; 
 
		case SERVICE_CONTROL_CONTINUE: 
		// Do whatever it takes to continue here. 
		MyServiceStatus.dwCurrentState = SERVICE_RUNNING; 
		break; 
 
		case SERVICE_CONTROL_STOP: 
		// Do whatever it takes to stop here. 
		MyServiceStatus.dwWin32ExitCode = 0; 
		MyServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
		MyServiceStatus.dwCheckPoint    = 0; 
		MyServiceStatus.dwWaitHint      = 0; 
		ReportEventLog (MSG_SERVICE_STOPPED, INFO);
		if (!SetServiceStatus (MyServiceStatusHandle,&MyServiceStatus)) { 
			status = GetLastError(); 
		} 
		return; 
 
		case SERVICE_CONTROL_INTERROGATE:
			{
			}
			break;

		default:
			{
			}
			break;
	} 
 
	// Send current status. 
	if (!SetServiceStatus (MyServiceStatusHandle,  &MyServiceStatus)) { 
		status = GetLastError(); 
	} 
	return; 
} 

void WINAPI MyServiceStart (DWORD argc, LPTSTR *argv) { 
	DWORD status; 
	DWORD specificError; 
 
	MyServiceStatus.dwServiceType = SERVICE_WIN32; 
	MyServiceStatus.dwCurrentState = SERVICE_START_PENDING; 
	MyServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE; 
	MyServiceStatus.dwWin32ExitCode = 0; 
	MyServiceStatus.dwServiceSpecificExitCode = 0; 
	MyServiceStatus.dwCheckPoint = 0; 
	MyServiceStatus.dwWaitHint = 0; 
 
	MyServiceStatusHandle = RegisterServiceCtrlHandler(g_szServiceID, MyServiceCtrlHandler); 
 
	if (MyServiceStatusHandle == (SERVICE_STATUS_HANDLE)0) { 
		return; 
	} 
 
	// Initialization code goes here. 
	status = MyServiceInitialization(argc,argv, &specificError); 
 
	// Handle error condition 
	if (status != NO_ERROR) { 
		MyServiceStatus.dwCurrentState = SERVICE_STOPPED; 
		MyServiceStatus.dwCheckPoint = 0; 
		MyServiceStatus.dwWaitHint = 0; 
		MyServiceStatus.dwWin32ExitCode = status; 
		MyServiceStatus.dwServiceSpecificExitCode = specificError; 
		SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus); 
		return;
	}
 
	// Initialization complete - report running status. 
	MyServiceStatus.dwCurrentState = SERVICE_RUNNING;
	MyServiceStatus.dwCheckPoint = 0; 
	MyServiceStatus.dwWaitHint = 0; 
 
	if (!SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus)) { 
		status = GetLastError(); 
	} 
 
	// This is where the service does its work. 
	ServiceMain ();
	return; 
} 
 
// Stub initialization function. 
DWORD MyServiceInitialization(DWORD   argc, LPTSTR  *argv, DWORD *specificError) { 
    argv;
    argc; 
    specificError;
    return(0);
}
 
int ServiceMain ()
{
	AddEventSource ("Application", g_szEventsource, 0); // Add the event log source to the registry - needed so the windows event log can view event descriptions.
	ReportEventLog (MSG_SERVICE_STARTED, INFO); // Report the service started event
	
	return 0;
}

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	char szCommand[SIZE_STRING];
	ZeroMemory (szCommand, SIZE_STRING);
	strncpy_s (szCommand, SIZE_STRING, lpCmdLine, 2);

	bool bUsingcommand = false;
	if (strcmp (szCommand, "/i") == 0) {
		// Install the service
		bUsingcommand = true;
		char szModulefilename[SIZE_STRING];
		ZeroMemory (szModulefilename, SIZE_STRING);
		GetModuleFileName(NULL, szModulefilename, SIZE_STRING);
		//strcat_s (szModulefilename, SIZE_STRING, " /s");

		InstallService (szModulefilename);
	}

	if (strcmp (szCommand, "/s") == 0) {
		// Start the service
		bUsingcommand = true;

	}

	if (strcmp (szCommand, "/d") == 0) {
		// Delete the service
		bUsingcommand = true;
		UninstallService ();
	}

	if (strcmp (szCommand, "/r") == 0) {
		// Run as application
		bUsingcommand = true;
		ServiceMain();
	}

	
	SERVICE_TABLE_ENTRY   DispatchTable[] = {{ TEXT(g_szServiceID), MyServiceStart}, {NULL,NULL}}; 
 
	if (bUsingcommand == false) {
		if (!StartServiceCtrlDispatcher( DispatchTable))  { 
			
		}
	}
}

void ReportEventLog (DWORD dwEventID, unsigned int iType)
{
    HANDLE h; 

    // Get a handle to the event log.

    h = RegisterEventSource(NULL, g_szEventsource);
    if (h == NULL) 
    {
        return;
    }

    // Report the event.
	WORD wType = EVENTLOG_INFORMATION_TYPE;

	if (iType == ERR) {
		wType = EVENTLOG_ERROR_TYPE;
	}

	if (iType == WARNING) {
		wType = EVENTLOG_WARNING_TYPE;
	}

	if (iType == INFO) {
		wType = EVENTLOG_INFORMATION_TYPE;
	}

    if (!ReportEvent(h,           // event log handle 
            wType,  // event type 
            0,            // event category  
            dwEventID,            // event identifier 
            NULL,                 // no user security identifier 
            0,             // number of substitution strings 
            0,                    // no data 
            NULL,                // pointer to strings 
            NULL))                // no data 
    {
        //printf("Could not report the event."); 
    }
 
    DeregisterEventSource(h); 
    return;
}

BOOL AddEventSource(LPTSTR pszLogName, LPTSTR pszSrcName, DWORD dwNum)
{
   HKEY hk; 
   DWORD dwData, dwDisp; 
   TCHAR szBuf[MAX_PATH];
   char pszMsgDLL[SIZE_STRING];

   ZeroMemory (pszMsgDLL, SIZE_STRING);
   GetModuleFileName (NULL, pszMsgDLL, SIZE_STRING);

   // Create the event source as a subkey of the log. 
 
   wsprintf(szBuf, 
      "SYSTEM\\CurrentControlSet\\Services\\EventLog\\%s\\%s",
      pszLogName, pszSrcName); 
 
   if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, szBuf, 
          0, NULL, REG_OPTION_NON_VOLATILE,
          KEY_WRITE, NULL, &hk, &dwDisp)) 
   {
      printf("Could not create the registry key."); 
      return FALSE;
   }
 
   // Set the name of the message file. 
 
   if (RegSetValueEx(hk,              // subkey handle 
           "EventMessageFile",        // value name 
           0,                         // must be zero 
           REG_EXPAND_SZ,             // value type 
           (LPBYTE) pszMsgDLL,        // pointer to value data 
           (DWORD) lstrlen(pszMsgDLL)+1)) // length of value data 
   {
      printf("Could not set the event message file."); 
      RegCloseKey(hk); 
      return FALSE;
   }
 
   // Set the supported event types. 
 
   dwData = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | 
        EVENTLOG_INFORMATION_TYPE; 
 
   if (RegSetValueEx(hk,      // subkey handle 
           "TypesSupported",  // value name 
           0,                 // must be zero 
           REG_DWORD,         // value type 
           (LPBYTE) &dwData,  // pointer to value data 
           sizeof(DWORD)))    // length of value data 
   {
      printf("Could not set the supported types."); 
      RegCloseKey(hk); 
      return FALSE;
   }
 
   // Set the category message file and number of categories.

   if (RegSetValueEx(hk,              // subkey handle 
           "CategoryMessageFile",     // value name 
           0,                         // must be zero 
           REG_EXPAND_SZ,             // value type 
           (LPBYTE) pszMsgDLL,        // pointer to value data 
           (DWORD) lstrlen(pszMsgDLL)+1)) // length of value data 
   {
      printf("Could not set the category message file."); 
      RegCloseKey(hk); 
      return FALSE;
   }
 
   if (RegSetValueEx(hk,      // subkey handle 
           "CategoryCount",   // value name 
           0,                 // must be zero 
           REG_DWORD,         // value type 
           (LPBYTE) &dwNum,   // pointer to value data 
           sizeof(DWORD)))    // length of value data 
   {
      printf("Could not set the category count."); 
      RegCloseKey(hk); 
      return FALSE;
   }

   RegCloseKey(hk); 
   return TRUE;
}


VOID InstallService (LPCTSTR lpszBinaryPathName) {
	
	// The below is not supported in windows NT - we have to take this out.
	SERVICE_DESCRIPTION scDesc = {g_szServiceDesc};
	
	SC_HANDLE schSCManager = OpenSCManager(NULL, NULL,  SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL) {
		return;
	}

	SC_HANDLE schService = CreateService(
		schSCManager,              // SCManager database 
		g_szServiceName,        // name of service 
		g_szFriendlyName,           // service name to display 
		SERVICE_ALL_ACCESS,        // desired access 
		SERVICE_WIN32_OWN_PROCESS, // service type 
		SERVICE_AUTO_START,      // start type 
		SERVICE_ERROR_NORMAL,      // error control type 
		lpszBinaryPathName,        // service's binary 
		NULL,                      // no load ordering group 
		NULL,                      // no tag identifier 
		NULL,                      // no dependencies 
		NULL,                      // LocalSystem account 
		NULL);                     // no password 
 
	if (schService == NULL) {
	
	} else {
		if (ChangeServiceConfig2 (schService, SERVICE_CONFIG_DESCRIPTION, &scDesc) == 0) {
		//	MessageBox (NULL, "Could not set Description.", "Error", MB_OK | MB_ICONEXCLAMATION); // Not Supported on Windows NT.
		}
		//MessageBox (NULL, "COE Desktop Alert Service Installed Successfully.", "Info", MB_ICONINFORMATION | MB_OK);
	}
 
	CloseServiceHandle(schService); 
}

VOID UninstallService () {

	SC_HANDLE schSCManager = OpenSCManager(NULL, NULL,  SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL) {
		return;
	}

	SC_HANDLE schService = OpenService (schSCManager, g_szServiceName, STANDARD_RIGHTS_REQUIRED);
	
	if (schService == NULL) {
		return;
	}
	
	if (DeleteService (schService) == 0) {
		
	} else {
		//MessageBox (NULL, "COE Desktop Alert Service Removed Successfully.", "Info", MB_ICONINFORMATION | MB_OK);
	}
	
	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager); 
}

VOID SvcDebugOut(LPSTR String, DWORD Status) { 
	CHAR  Buffer[1024]; 
	if (strlen(String) < 1000) { 
		sprintf(Buffer, String, Status); 
		OutputDebugStringA(Buffer); 
	} 
}

