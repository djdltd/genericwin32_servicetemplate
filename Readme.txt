This is a template for building Windows Services using Win32. I would recommend building
services by using this template as a starting point. It has the following features already built in:

Installing / UnInstalling the service (/i and /d switches)
Running the service as an application (/r switch)
Using the Windows Event Log. (Add event descriptions to the MC file)

The Service code starts with ServiceMain() after running.


